package br.edu.vianna.ex_01_carronovo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import br.edu.vianna.ex_01_carronovo.models.Carro;

public class MainActivity extends AppCompatActivity {
    private FloatingActionButton fbtnSave;

    // Características do carro:
    private EditText txtValor;
    private Switch swtImportado, swtMotor1000, swtAr, swtAuto, swtAlarme, swtPinturaEspecial,
            swtTetoSolar, swtKitMultimidia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindings();

        fbtnSave.setOnClickListener(callSaveCar());
    }

    private View.OnClickListener callSaveCar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!txtValor.getText().equals("")) {
                    Carro carro = new Carro(
                            Double.parseDouble(txtValor.getText().toString()),
                            swtImportado.isChecked(),
                            swtMotor1000.isChecked(),
                            swtAr.isChecked(),
                            swtAuto.isChecked(),
                            swtAlarme.isChecked(),
                            swtPinturaEspecial.isChecked(),
                            swtTetoSolar.isChecked(),
                            swtKitMultimidia.isChecked()
                    );
                    Intent i = new Intent(getApplicationContext(), NewCarActivity.class);
                    i.putExtra("novoCarro", carro);

                    startActivityForResult(i, 1);
                }
            }
        };
    }

    private void bindings() {
        fbtnSave = findViewById(R.id.fbtnSave);
        txtValor = findViewById(R.id.txtValor);
        swtImportado = findViewById(R.id.swtImportado);
        swtMotor1000 = findViewById(R.id.swtMotor1000);
        swtAr = findViewById(R.id.swtAr);
        swtAuto = findViewById(R.id.swtAuto);
        swtAlarme = findViewById(R.id.swtAlarme);
        swtPinturaEspecial = findViewById(R.id.swtPinturaEspecial);
        swtTetoSolar = findViewById(R.id.swtTetoSolar);
        swtKitMultimidia = findViewById(R.id.swtKitMultimidia);
    }
}