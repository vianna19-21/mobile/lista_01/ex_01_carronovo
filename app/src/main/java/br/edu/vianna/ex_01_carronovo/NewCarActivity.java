package br.edu.vianna.ex_01_carronovo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import br.edu.vianna.ex_01_carronovo.models.Carro;

public class NewCarActivity extends AppCompatActivity {
    private Carro carro;
    private TextView txtValor, txtImportado, txtMotor1000, txtAr, txtAuto, txtAlarme, txtPinturaEspecial,
            txtTetoSolar, txtKitMultimidia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_car);
        
        bindings();
        carro = (Carro) getIntent().getExtras().getSerializable("novoCarro");

        detalhaCarroEmTela(carro);

        System.out.println("Valor do Carro: " + carro.getValor());
    }

    private void detalhaCarroEmTela(Carro carro) {
        txtValor.setText(String.valueOf(carro.calculaValorFinal()));
        txtImportado.setText(carro.isImportado() ? "Sim" : "Não");
        txtMotor1000.setText(carro.hasMotor1000() ? "Sim" : "Não");
        txtAr.setText(carro.hasAr() ? "Sim" : "Não");
        txtAuto.setText(carro.hasAuto() ? "Sim" : "Não");
        txtAlarme.setText(carro.hasAlarme() ? "Sim" : "Não");
        txtPinturaEspecial.setText(carro.hasPinturaEspecial() ? "Sim" : "Não");
        txtTetoSolar.setText(carro.hasTetoSolar() ? "Sim" : "Não");
        txtKitMultimidia.setText(carro.hasKitMultimidia() ? "Sim" : "Não");
    }

    private void bindings() {
        txtValor = findViewById(R.id.lblValorValue);
        txtImportado = findViewById(R.id.lblImportadoValue);
        txtMotor1000 = findViewById(R.id.lblMotor1000Value);
        txtAr = findViewById(R.id.lblArValue);
        txtAuto = findViewById(R.id.lblAutoValue);
        txtAlarme = findViewById(R.id.lblAlarmeValue);
        txtPinturaEspecial = findViewById(R.id.lblPinturaEspecialValue);
        txtTetoSolar = findViewById(R.id.lblTetoSolarValue);
        txtKitMultimidia = findViewById(R.id.lblKitMultimidiaValue);
    }
}