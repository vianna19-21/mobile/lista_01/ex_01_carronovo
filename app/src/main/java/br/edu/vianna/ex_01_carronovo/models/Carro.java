package br.edu.vianna.ex_01_carronovo.models;

import java.io.Serializable;

public class Carro implements Serializable {
    private double valor;
    private boolean importado, motor1000;
    private boolean ar, auto, alarme, pinturaEspecial, tetoSolar, kitMultimidia;

    public Carro() {
    }

    public Carro(double valor, boolean importado, boolean motor1000, boolean ar, boolean auto, boolean alarme, boolean pinturaEspecial, boolean tetoSolar, boolean kitMultimidia) {
        this.valor = valor;
        this.importado = importado;
        this.motor1000 = motor1000;
        this.ar = ar;
        this.auto = auto;
        this.alarme = alarme;
        this.pinturaEspecial = pinturaEspecial;
        this.tetoSolar = tetoSolar;
        this.kitMultimidia = kitMultimidia;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isImportado() {
        return importado;
    }

    public void setImportado(boolean importado) {
        this.importado = importado;
    }

    public boolean hasMotor1000() {
        return motor1000;
    }

    public void setMotor1000(boolean motor1000) {
        this.motor1000 = motor1000;
    }

    public boolean hasAr() {
        return ar;
    }

    public void setAr(boolean ar) {
        this.ar = ar;
    }

    public boolean hasAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public boolean hasAlarme() {
        return alarme;
    }

    public void setAlarme(boolean alarme) {
        this.alarme = alarme;
    }

    public boolean hasPinturaEspecial() {
        return pinturaEspecial;
    }

    public void setPinturaEspecial(boolean pinturaEspecial) {
        this.pinturaEspecial = pinturaEspecial;
    }

    public boolean hasTetoSolar() {
        return tetoSolar;
    }

    public void setTetoSolar(boolean tetoSolar) {
        this.tetoSolar = tetoSolar;
    }

    public boolean hasKitMultimidia() {
        return kitMultimidia;
    }

    public void setKitMultimidia(boolean kitMultimidia) {
        this.kitMultimidia = kitMultimidia;
    }

    public double calculaValorFinal() {
        double valorFinal = valor;

        if (hasAr()) { valorFinal += 3000; }
        if (hasAuto()) { valorFinal += 5000; }
        if (hasAlarme()) { valorFinal += 800; }
        if (hasPinturaEspecial()) { valorFinal += 2500; }
        if (hasTetoSolar()) { valorFinal += 4000; }
        if (hasKitMultimidia()) { valorFinal += 1800; }

        if (hasMotor1000()) {
            valorFinal = valorFinal * 1.1;
        } else {
            valorFinal = valorFinal * 1.2;
        }

        if (isImportado()) { valorFinal = valorFinal * 1.3; };

        return valorFinal;
    }
}
